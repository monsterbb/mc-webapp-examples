<?php
include("common.php");

// ------------------------------------------------------------- preperation steps beign
$env['PATH_TRANSLATED'] = '';
$_SERVER['PATH_TRANSLATED'] = '';
// ------------------------------------------------------------- preperation steps end


// session tests first due to the headers
test_session();


test_iconv();
test_openssl_1();

// ------------------------------------------------------------- directory access tests begin
test_dir_access_1();
test_dir_access_2();
test_dir_access_3();
test_dir_access_4();
test_dir_access_5();
test_dir_access_6();
test_dir_access_7();
test_dir_access_8();
// ------------------------------------------------------------- directory access tests end

test_pear();
test_zip();

// ------------------------------------------------------------- testing name resolution begin
test_name_resolution_1();
test_name_resolution_2();
// ------------------------------------------------------------- testing name resolution end

test_file_upload();

test_xml_classes();

// ------------------------------------------------------------- prestigo dependencies begin
test_prestigo_1();
test_prestigo_2();
test_prestigo_3();
// ------------------------------------------------------------- prestigo dependencies end

// ------------------------------------------------------------- mysql begin
test_mysql();
test_mysqli();
test_pdo_mysql();
// ------------------------------------------------------------- mysql end

test_tmp_write_access();

if($execute_mail_tests) {
  // ------------------------------------------------------------- mail begin
  test_mail_external();
  test_mail_external_eval();
  test_mail_mailbox();
  test_mail_alias();
  test_mail_mailbox_plus_alias();  
  // ------------------------------------------------------------- mail end

  test_curl_selfsigned();
  test_curl_valid();
  test_curl_pwprotected();

}else{
  add_test("external tests (mail+badssl) skipped to prevent flooding", true);
}

show_test_results();
exit();


function test_curl_selfsigned(){
  $a = fetch_https_url("https://self-signed.badssl.com/");
  add_test("self signed certificates shall be rejected by default", $a == 'SSL certificate problem: self signed certificate');
}

function test_curl_valid(){
  $a = fetch_https_url("https://sha256.badssl.com/");
  add_test("tls trust should be working with valid remote certificate", 0 < strpos($a, 'background: green;'));
}

function test_curl_pwprotected(){
  global $config;
  $urlbase = "http://{$_SERVER['HTTP_HOST']}/protected";
  $url = "$urlbase/static.html";
  $a = fetch_https_url($url);
  add_test("directory shall be password protected", 0 < strpos($a, "Authorization Required"), $a);

  $a = fetch_https_url($url, "admin", $config["access_password"]);
  add_test("static content shall be served correctly from pw protected directories", $a == "static content", $a);

  $expected = md5("admin");
  $url = "$urlbase/dynamic.php";
  $a = fetch_https_url($url, "admin", $config["access_password"]);
  add_test("password protected directory and dynamic content (also testing if the username is relayer)", $a == $expected, $a);
}

function fetch_https_url($url, $username="", $password="") {
  $ch = curl_init();
  // Disable SSL verification
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set the url
  curl_setopt($ch, CURLOPT_URL,$url);
  // auth if provided
  if($password) {
     curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
     curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
  }
  // Execute
  $result=curl_exec($ch);
  if($result === false) 
    $result = curl_error($ch);
  curl_close($ch);
  return $result;
}

function test_file_upload(){
    global $config;

    $pic = "jpeg-as-php.php.jpg";
    @unlink("../$pic");

    $target_url = "http://${_SERVER['HTTP_HOST']}/upload-test.php";
    $file_name_with_full_path = realpath($pic);

    $ch = curl_init();

    if (function_exists('curl_file_create')) { // php 5.5+
      $cFile = curl_file_create($file_name_with_full_path);
    } else { // 
      @curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
      $cFile = '@' . $file_name_with_full_path;
    }
    $post = array(
      'access_password' => $config["access_password"],
      'submit' => 'ok', 
      'target_dir' => '..', 
      'upfile' => $cFile
    );

    curl_setopt($ch, CURLOPT_URL,$target_url);
    curl_setopt($ch, CURLOPT_POST,1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $result=curl_exec ($ch);
    curl_close ($ch);
    #echo $result;

    add_test("file upload", file_exists("../$pic"), $result);
}

function hasResolvConfSearchDomainEntries() {
  $lines = @file("/etc/resolv.conf");
  $re = current(array_filter($lines, function($element) { 
     return preg_match('/^(domain|search) /', $element);
  }));
  // var_dump($lines, $re);
  return $re;
}

function test_mysql(){
  global $config;
  if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
     add_test("mysql_connect test skipped, PHP is recent", true);
     return;
  }

  $link = mysql_connect($config["db_hostname"], $config["db_username"], $config["db_password"]);
  $r = mysql_select_db($config["db_database"], $link);
  add_test("mysql_connect shall work on old PHPs", $r);
  mysql_close($link);
}

function test_mysqli(){
  global $config;
  $mysqli = new mysqli($config["db_hostname"], $config["db_username"], $config["db_password"], $config["db_database"]);

  add_test("mysqli connection", !$mysqli->connect_error, $mysqli->connect_error);

  $mysqli->close();
}

function test_pdo_mysql(){
  global $config;
  $success = false;
  try{
     $db = new PDO('mysql:host='.$config["db_hostname"].';dbname='.$config["db_database"].';charset=utf8mb4', $config["db_username"], $config["db_password"]);
     $success = true;
  } catch(Exception $e) {
    $ge = $e;
  }

  add_test("PDO::mysql connection", $success, $ge);
}

function test_iconv(){
  $translated = iconv("UTF-8", "ISO-8859-1//TRANSLIT", "This is the Euro symbol '€'.");
  add_test("iconv translition", $translated == "This is the Euro symbol 'EUR'.", $translated);
}

function test_openssl_1(){
  if (version_compare(PHP_VERSION, '5.6.0') < 0) {
     add_test("skipping openssl test, PHP is too old", true);
     return;
  }

  if(!function_exists("openssl_pkey_get_details"))
    $rsa["failure"] = true;
  else
$rsa = openssl_pkey_get_details(openssl_pkey_get_public("
-----BEGIN CERTIFICATE-----
MIIFTTCCBDWgAwIBAgIHJ4ZocC8+VDANBgkqhkiG9w0BAQUFADCByjELMAkGA1UE
BhMCVVMxEDAOBgNVBAgTB0FyaXpvbmExEzARBgNVBAcTClNjb3R0c2RhbGUxGjAY
BgNVBAoTEUdvRGFkZHkuY29tLCBJbmMuMTMwMQYDVQQLEypodHRwOi8vY2VydGlm
aWNhdGVzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkxMDAuBgNVBAMTJ0dvIERhZGR5
IFNlY3VyZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTERMA8GA1UEBRMIMDc5Njky
ODcwHhcNMTMwNTE4MTgzNTMwWhcNMTUwNTE4MTgzNTMwWjA9MSEwHwYDVQQLExhE
b21haW4gQ29udHJvbCBWYWxpZGF0ZWQxGDAWBgNVBAMTD21vbnN0ZXJtZWRpYS5o
dTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMDgPqkYwvrDs3HD9BD1
fwq05RXrrB8BK1T+2QkuOE3s3pvx8wW8VMLum+p/6YWwLE8lNvQKMnVGLwU5z8F1
dO6cRDcPDTLts10GlOPKRBo3ITrSY4eQsoLz/HkJAWZbSLTtCJZJpmG2cvJnIX9K
iT0GPY1q5uzg1o2tSLLrlOd1YPdofpiOm88J5BjNJnGV6ql18Mzqlo+60R7LJ9nW
q6/pgxPIXGGBzAk5mSp2xjp2x2e8HrBXMyi+ZfolJ/w0r9oZdSZb3y0OilDRAR9V
e9JBEmlQNAMyIGLVH3caS0W2WbE2nKqzs2HQGOjeB3xynZ2qryCebTYdepy1Ye1+
pmkCAwEAAaOCAcIwggG+MA8GA1UdEwEB/wQFMAMBAQAwHQYDVR0lBBYwFAYIKwYB
BQUHAwEGCCsGAQUFBwMCMA4GA1UdDwEB/wQEAwIFoDAzBgNVHR8ELDAqMCigJqAk
hiJodHRwOi8vY3JsLmdvZGFkZHkuY29tL2dkczEtOTEuY3JsMFMGA1UdIARMMEow
SAYLYIZIAYb9bQEHFwEwOTA3BggrBgEFBQcCARYraHR0cDovL2NlcnRpZmljYXRl
cy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5LzCBgAYIKwYBBQUHAQEEdDByMCQGCCsG
AQUFBzABhhhodHRwOi8vb2NzcC5nb2RhZGR5LmNvbS8wSgYIKwYBBQUHMAKGPmh0
dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29kYWRkeS5jb20vcmVwb3NpdG9yeS9nZF9pbnRl
cm1lZGlhdGUuY3J0MB8GA1UdIwQYMBaAFP2sYTKTbEXW4u6FX5q653aZaMznMC8G
A1UdEQQoMCaCD21vbnN0ZXJtZWRpYS5odYITd3d3Lm1vbnN0ZXJtZWRpYS5odTAd
BgNVHQ4EFgQUu7lepjRQyzsE800y/2QudhBVZgAwDQYJKoZIhvcNAQEFBQADggEB
ALTMvwQVfSxb3ghNa7LhmQW/wM8JG5rlvqCGLYQe/rBZ0lrCseZGrI435SF8bmx3
ygrYNBzvYyvhOSA+VNicHplDVTJMvpK3fJEvFDqqoswRTWogqeHNe1I/yrx+tDRd
74+qMckEc08uUhscuP0Q9LvhSQA4ovyKqyxIDUtVxVqLFEdoQJBIW7q1u5lHZ5Dm
oB2nv2BJa6vb9iJstKn2fCcnhi81G7+BUmO2MIXfXJDn7oZirszrogyUlZaE2MGM
KPMeVhmcEdZhAZMU3KKFohH0r8JX5/MEubwkB58adh9xnbhf4lDnqwFzNYR1wl3L
Idcr6pe87Tu8YLdFi6V8R7Q=
-----END CERTIFICATE-----
"));


  add_test("openssl certificate parsing", $rsa["bits"] == 2048, $rsa);
}

function test_dir_access_1(){
  add_test("host filesystem visibility (raw)", false === is_file("/etc/smartd.conf"));
}

function test_dir_access_2(){
  add_test("host filesystem visibility (symlink)", false === is_file("/link/etc/smartd.conf"));
}

function test_dir_access_3(){
  add_test("reslog check", false === is_dir("/reslog"));
}

function test_dir_access_4(){
  add_test("root must be a directory", true === is_dir("/"));
}

function test_dir_access_5(){
  $d = scandir("../../..");
  add_test("scandir root", 5 <= count($d), $d);
}

function test_dir_access_6(){
  $d = scandir("../pages");
  add_test("scandir pages", 5 <= count($d), $d);
}

function test_dir_access_7(){
  add_test("scandir non.existent dir", false === @scandir("nonexistent"));
}

function test_dir_access_8(){
  add_test("scandir on symlink shall work", in_array("var", scandir("link")));
}
function test_pear(){
  $success = false;
  try {
    if(include("PEAR.php"))
      $success = true;
  }catch(Exception $e) {
     $ge = $e; 
  }
  add_test("PEAR shall be available", $success, $ge);
}

function test_name_resolution_1(){
  $host = "index.hu";
  $resolved = gethostbyname($host);

  add_test("name resolution shall work", $resolved != $host);
}
function test_name_resolution_2(){
  add_test("search entries shall not be present in /etc/resolv.conf", !hasResolvConfSearchDomainEntries());
}
function test_session(){
  add_test("session_start shall work", session_start());

  if(!$_SESSION["i"]) $_SESSION["i"] = 0;
  $_SESSION["i"]++;
  $cur = $_SESSION["i"];
  $sid = session_id();
  session_write_close();
  $rs = (int) file_get_contents("http://${_SERVER["HTTP_HOST"]}/session.php?sid=$sid");
  add_test("session counter increase test", $cur < $rs, Array("cur"=>$cur, "rs"=>$rs));
}

function test_xml_classes(){
  $success = false;
  try {
    new XMLWriter();
    new XMLReader();

    $success = true;
  }catch(Exception $e){
    $ge = $e;
  }
  add_test("XMLReader and XMLWriter classes shall be available", $success, $ge);
}

function test_prestigo_1(){
  if (version_compare(PHP_VERSION, '5.6.0') < 0) {
     add_test("skipping finfo test, PHP is too old", true);
     return;
  }
  $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
  $r = finfo_file($finfo, "test.php");
  add_test("finfo shall be avaialble for prestigo", "text/x-php" == $r, $r);
  finfo_close($finfo);
}

function test_prestigo_2(){
  if (version_compare(PHP_VERSION, '5.6.0') < 0) {
     add_test("skipping posix test, PHP is too old", true);
     return;
  }
  $pid = posix_getpid();
  add_test("posix functions shall be available for prestigo", isset($pid) and $pid > 0, $pid);
}

function test_prestigo_3(){
  if (version_compare(PHP_VERSION, '5.6.0') < 0) {
     add_test("skipping collator test, PHP is too old", true);
     return;
  }

  $coll = collator_create('en_US');
  $cmp1 = collator_compare($coll, "string#1", "string#2");
  $cmp2 = collator_compare($coll, "string", "string");
  $cmp3 = collator_compare($coll, "string#2", "string#1");
  add_test("intl module is needed for prestigo", ($cmp1 === -1) and ($cmp2 === 0) and ($cmp3 === 1));
}

function test_mail_external(){
  global $config;

  $m = mail($config["email_to_external"], "test subject ".$_SERVER['HTTP_HOST'], "this is a test message from ${_SERVER['HTTP_HOST']}", "From: ".$config["email_from"]);
  add_test("mail() function shall work by default", $m);
}


function test_mail_external_eval(){
  global $config;
  $str = '
  return mail("'.$config["email_to_external"].'", "test subject '.$_SERVER['HTTP_HOST'].'", "this is a test message from '.$_SERVER["HTTP_HOST"].'", "From: '.$config["email_from"].'");
  ';
  $re = false;
  try {
    $re = eval($str);
  } catch(Exception $e) {
    $ge = $e;
  }
  add_test("mail() function shall work in eval (some mail applications expect this)", $re, $ge);
}

function test_mail_mailbox(){
  global $config;

  sendmail_fetch_test($config["email_account_1"], "standard mailbox test", $config["email_account_1"]);
}

function test_mail_alias(){
  global $config;

  sendmail_fetch_test($config["email_alias_1"], "email alias test", $config["email_account_1"]);
}

function test_mail_mailbox_plus_alias(){
  global $config;

  sendmail_fetch_test($config["email_account_2"], "email mailbox plus alias test", $config["email_account_2"], $config["email_account_1"]);
}

function sendmail_fetch_test($destemail, $remark, $checkemail, $checkemail2="") {
  global $config;

  $now = time();
  $subject = $_SERVER["HTTP_HOST"]." $remark $now";
  $m = mail($destemail, $subject, "this is a test message from ${_SERVER['HTTP_HOST']}", "From: ".$config["email_from"]);
  add_test("mail should be accepted for submission: $destemail: $remark", $m);

  fetch_mail_test($checkemail, $subject, $remark);
  if($checkemail2) {
    fetch_mail_test($checkemail2, $subject, $remark);
  }
}

function fetch_mail_test($email, $subject, $remark) {
  global $config;
  $err = imap_expect_message("{".$config["email_account_hostname"].":143/imap/notls}", $email, $config["email_account_password"], $subject);
  add_test("mail should be delivered to the target mailbox $email: $remark", $err == "", $err);
}

function imap_expect_message($hostname, $username, $password, $expected_subject, $max_attempts = 15) {
  $tries = 0;
  $subject_found = false;
  while(!$subject_found) {
    $inbox = imap_open($hostname,$username,$password);
    if(!$inbox) {
      return "Unable to open IMAP connection";
    }

    try {
        $nums=imap_num_msg($inbox);
        debug("IMAP mails in $username: $nums");

        for ($i=1; $i<=$nums; $i++){
          $overview = imap_fetch_overview($inbox, $i, 0);
          $o = $overview[0];
          debug("IMAP mail #$i: ".print_r($o, true));
          if($o->subject == $expected_subject) {
             $subject_found = true;
             imap_delete($inbox, $o->uid, FT_UID);
          }
        }



    } catch(Exception $e) {
      // note this block is here for php <5.5 compatibility
      echo $e;
    }
    if(!$subject_found) {
      // echo "subject: $expected_subject not found\n";
      $tries++;
      if($tries < $max_attempts) {
        sleep(1);
      } else {
        break;
      }
    }
    else {
      imap_expunge($inbox);
    }

    imap_close($inbox);
  }
  return !$subject_found ? "Max attempts reached, mail not found, giving up.\n" : "";

}

function debug($msg){
  if(!$_GET["debug"]) return;
  echo "$msg<br>\n";
}

function test_zip() {
  $za = new ZipArchive();
  $za->open('../test.zip');
  $f = $za->statIndex(0);
  $za->close();
  add_test("zip files support", $f["name"] == "README.md", print_r($f, true));
}

?>