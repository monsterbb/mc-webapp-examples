<?php

$config = parse_ini_file("../../hacktest-config.ini");
if(!$config) die("Configuration file not found.");

if(!$config["access_password"]) die("access_password not configured");
if(strlen($config["access_password"]) < 8) die("access_password is weak");

if($config["access_password"] != $_REQUEST["access_password"]) die("Access denied");

$tests_ok = 0; $tests_failed = 0;

$mtime_path = "../../hacktest-mail-${_SERVER['HTTP_HOST']}.mtime";
$now = time();
$last = read_last_mtime();
$execute_mail_tests = $_GET["force"] || false;
if((!$last)||($last < $now - 6*60*60)) {
  write_new_mtime($now);
  $execute_mail_tests = true;
}

function add_test($name, $result, $additional=null) {
  global $tests_ok, $tests_failed;
  if($result) $tests_ok++; else $tests_failed++;
  $tests = $tests_ok+$tests_failed;
  echo "<div>Test #$tests: $name: ".($result ? "OK" : ("FAILED".($additional? " ". (print_r($additional,true)) :"" )) )."</div>";
}

function show_test_results(){
  global $tests_ok, $tests_failed;
  echo "<br/>";
  echo "<div>Tests succeded: $tests_ok</div>";
  echo "<div>Tests failed: $tests_failed</div>";
  if(!$tests_failed)
    echo "<div>Checkout green.</div>";
}

function test_tmp_write_access(){
   $d = "writetest";
   $fn = "/tmp/writetest.txt";
   $w = file_put_contents($fn, $d);
   unlink($fn);
   add_test("tmp write test (needed for sending out emails)", $w === strlen($d), $d);   
}

function read_last_mtime(){
  global $mtime_path;
  return @file_get_contents($mtime_path) || 0;
}
function write_new_mtime($now){
  global $mtime_path;
  return @file_put_contents($mtime_path, $now);
}

?>