<?php
include("common.php");

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {

    print_r($_FILES);

    if(!$_FILES["upfile"]["name"])
      die("Filename was not sent");
    if($_FILES["upfile"]["error"])
      die("File upload was interrupted");

    $target_dir = $_POST["target_dir"];
    $filename = basename($_FILES["upfile"]["name"]);
    if(!preg_match('/\.(png|jpg|jpeg|gif)$/', $filename))
      die("Picture files only!");

    $target_file = $target_dir . "/". $filename;
    if(!move_uploaded_file($_FILES["upfile"]["tmp_name"], $target_file))
      die("Failed to save the file");

    $link = "http://${_SERVER['HTTP_HOST']}/$target_file";
    echo "Success. <a href='$link'>$link</a>";
    exit();
}

?>
<!DOCTYPE html>
<html>
<body>

<form method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="text" name="access_password">
    <input type="text" name="target_dir" value=".">
    <input type="file" name="upfile" id="upfile" accept="image/*">
    <input type="submit" value="Upload Image" name="submit">
</form>

</body>
</html>