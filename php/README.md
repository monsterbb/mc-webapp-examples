For PHP based web applications, all what you need to do is to start a container on top of the PHP image of the desired version.
The PHP scripts will be executed from the document root.

Document root for the vhost: `/php/pages`

PHP configuration options can be overridden by placing new `.ini` files to `/conf/php.d` or using [.user.ini](http://php.net/manual/en/configuration.file.per-user.php) files.
