import flask
from flask import Flask
application = Flask(__name__)

@application.route("/hello")
def hello():
    return "Hello from a Python Flask application (%s)!" % (flask.__version__)

if __name__ == "__main__":
    application.run(host='0.0.0.0')
