# README #

This project contains a bunch of example web applications that you can use on the Monster Media shared webhosting provider.

### What is this repository for? ###

In general, you need to configure the document root of the vhost entry as `/apptype/pages` (this is where the static resources will be served from)
and the path of main application directory is always `/apptype/app`.

For example in case of Python-Flask, you can use the following paths to launch the application:

- document root for the vhost: `/nodejs-express/pages`
- application directory: `/nodejs-express/app`

These path names are not hard requirements to make your web applications work, rather just recommendations: 
You can deploy your application to different paths, if you prefer.

### Who do I talk to? ###

* https://www.monstermedia.hu
* https://info.monstermedia.hu
