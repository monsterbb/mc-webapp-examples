# README #

To launch this project, you'll need to specify the following variables:

Document root for the vhost: `/python-django/pages`

Application directory: `/python-django/app`
Subdirectory: `mysite`
WSGI project name: `mysite.wsgi`
