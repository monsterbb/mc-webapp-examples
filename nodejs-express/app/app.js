const listenSocketPath = '/var/run/mc-app-socket/socket';

const express = require('express');
const app = express();
const fs = require("fs");

// if the unix socket already exists, it needs to be removed first
if (fs.existsSync(listenSocketPath)) {
    fs.unlinkSync(listenSocketPath);
}

app.get('/hello', (req, res) => res.send('Hello World from a Node.JS+Express application!'));

app.listen(listenSocketPath, () => console.log('Example app listening!'));